//
//  OAuthService.swift
//  OAuthTest
//
//  Created by Jeffbala on 2018/11/20.
//  Copyright © 2018年 Jeffbala. All rights reserved.
//

import UIKit
import OAuthSwift

struct OAuthService {
    
    typealias OAuthServiceSucessHandler = (_ message: String) -> ()
    typealias OAuthServiceFailedHandler = (_ error: String) -> ()
    
    enum OAuthServiceError: String, Error {
        case invalidUrl = "Cannot construct a valid url string."
        case internalError = "Internal error."
    }
    
    private let viewController: UIViewController!
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
    
    func authorize(type: OAuthType, success: @escaping OAuthServiceSucessHandler, failure: @escaping OAuthServiceFailedHandler) {
        
        let oAuth2Info = OAuthInfo.buildOAuth2Info(type: type, viewController: viewController)
        let oauthswift = oAuth2Info.object
        
        guard
            let callbackUrlString = oAuth2Info.info[OAuthConfigKey.callbackUrl],
            let rwURL = URL(string: callbackUrlString),
            let scope = oAuth2Info.info[OAuthConfigKey.scope],
            let state = oAuth2Info.info[OAuthConfigKey.state],
            let requestUrlString = oAuth2Info.info[OAuthConfigKey.requestUrl]
        else {
            failure(OAuthServiceError.internalError.rawValue)
            return
        }
        
        oauthswift.authorize(withCallbackURL: rwURL, scope: scope, state: state, success: { (credential, response, parameters) in
            print("authorize response data: \(response?.string ?? "no data")")
            _ = oauthswift.client.get(requestUrlString, success: { (response) in
                let result = String(data: response.data, encoding: .utf8) ?? ""
                print("client get result: \(result)")
                success(result)
            }, failure: { (error) in
                failure(error.localizedDescription)
            })
        }) { (error) in
            failure(error.localizedDescription)
        }
    }
}
