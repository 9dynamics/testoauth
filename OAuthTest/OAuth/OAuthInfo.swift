//
//  OAuthInfo.swift
//  OAuthTest
//
//  Created by Jeffbala on 2018/11/20.
//  Copyright © 2018年 Jeffbala. All rights reserved.
//

import Foundation
import OAuthSwift

enum OAuthType {
    case google, facebook, line
}

enum OAuthConfigKey: String {
    case callbackUrl, scope, state, requestUrl
}

struct OAuthInfo {
    typealias OAuth2Info = (object: OAuth2Swift, info: [OAuthConfigKey: String])
    
    private struct OAuthConfig {
        struct Google {
            static let clientId = "256904099549-3g1as8epmass320ii8srb5sh1g641mff.apps.googleusercontent.com"
            static let consumerSecret = ""
            static let authorizeUrl = "https://accounts.google.com/o/oauth2/auth"
            static let accessTokenUrl = "https://accounts.google.com/o/oauth2/token"
            static let responseType = "code"
            static let callbackUrl = "\(Bundle.main.bundleIdentifier!):/oauth2Callback"
            static let scope = "https://www.googleapis.com/auth/drive"
            static let state = ""
            static let requestUrl = "https://www.googleapis.com/drive/v2/files"
        }
        // com.mgallery.OAuthTest:/oauth-callback
        struct Facebook {
            static let clientId = "887468734977065"
            static let consumerSecret = "693af0775ab4528049737e75551dd2c1"
            static let authorizeUrl = "https://www.facebook.com/dialog/oauth"
            static let accessTokenUrl = "https://graph.facebook.com/oauth/access_token"
            static let responseType = "code"
            static let callbackUrl = "https://oauthswift.herokuapp.com/callback/OAuthTest"
            static let scope = "public_profile, email"
            static let state = ""
            static let requestUrl = "https://graph.facebook.com/me?"
        }
        
        struct Line {
            
        }
    }
    
    static func buildOAuth2Info(type: OAuthType, viewController: UIViewController) -> OAuth2Info {
        let oauthswift: OAuth2Swift!
        var info: [OAuthConfigKey: String] = [:]
        
        switch type {
        case .google:
            oauthswift = OAuth2Swift(
                consumerKey: OAuthConfig.Google.clientId,
                consumerSecret: OAuthConfig.Google.consumerSecret,
                authorizeUrl: OAuthConfig.Google.authorizeUrl,
                accessTokenUrl: OAuthConfig.Google.accessTokenUrl,
                responseType:  OAuthConfig.Google.responseType
            )
            
            info[OAuthConfigKey.callbackUrl] = OAuthConfig.Google.callbackUrl
            info[OAuthConfigKey.scope] = OAuthConfig.Google.scope
            info[OAuthConfigKey.state] = OAuthConfig.Google.state
            info[OAuthConfigKey.requestUrl] = OAuthConfig.Google.requestUrl
            
        case .facebook:
            oauthswift = OAuth2Swift(
                consumerKey: OAuthConfig.Facebook.clientId,
                consumerSecret: OAuthConfig.Facebook.consumerSecret,
                authorizeUrl: OAuthConfig.Facebook.authorizeUrl,
                accessTokenUrl: OAuthConfig.Facebook.accessTokenUrl,
                responseType:  OAuthConfig.Facebook.responseType
            )
            
            info[OAuthConfigKey.callbackUrl] = OAuthConfig.Facebook.callbackUrl
            info[OAuthConfigKey.scope] = OAuthConfig.Facebook.scope
            info[OAuthConfigKey.state] = generateState(withLength: 20)
            info[OAuthConfigKey.requestUrl] = OAuthConfig.Facebook.requestUrl
            
        default:
            oauthswift = OAuth2Swift(consumerKey: "", consumerSecret: "", authorizeUrl: "", accessTokenUrl: "", responseType: "")
        }
        
        oauthswift.allowMissingStateCheck = true
        oauthswift.authorizeURLHandler = SafariURLHandler(viewController: viewController, oauthSwift: oauthswift)
        
        return (object: oauthswift, info: info)
    }
}
