//
//  OAuthController.swift
//  OAuthTest
//
//  Created by Jeffbala on 2018/11/20.
//  Copyright © 2018年 Jeffbala. All rights reserved.
//

import UIKit

class OAuthController: UIViewController {
    
    // MARK: - Private Properties
    
    private var oAuthView: OAuthView!
    private var oAuthService: OAuthService!
    
    // MARK: - Initialization
    
    init(networkManager: NetworkManager) {
        super.init(nibName: nil, bundle: nil)
        oAuthView = OAuthView(viewModel: OAuthViewModel())
        oAuthService = OAuthService(viewController: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycles
    
    override func loadView() {
        super.loadView()
        view = oAuthView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        oAuthView.delegate = self
    }
    
    deinit { print("OAuthController deinit") }
}

extension OAuthController: OAuthViewActionDelegate {
    func fbButtonTapped(viewModel: OAuthViewModel) {
        oAuthService.authorize(type: .facebook ,success: { (result) in
            self.oAuthView.update(message: result)
        }) { (error) in
            self.oAuthView.update(message: error)
        }
    }
    
    func lineButtonTapped(viewModel: OAuthViewModel) {
        oAuthView.update(message: "Line button tapped.")
    }
    
    func googleButtonTapped(viewModel: OAuthViewModel) {
        oAuthService.authorize(type: .google ,success: { (result) in
            self.oAuthView.update(message: result)
        }) { (error) in
            self.oAuthView.update(message: error)
        }
    }
}


