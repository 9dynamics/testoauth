//
//  OAuthView.swift
//  OAuthTest
//
//  Created by Jeffbala on 2018/11/20.
//  Copyright © 2018年 Jeffbala. All rights reserved.
//

import UIKit

protocol OAuthViewActionDelegate: class {
    func fbButtonTapped(viewModel: OAuthViewModel)
    func lineButtonTapped(viewModel: OAuthViewModel)
    func googleButtonTapped(viewModel: OAuthViewModel)
}

class OAuthView: UIView {
    
    // MARK: - Public APIs
    
    weak var delegate: OAuthViewActionDelegate?
    
    // MARK: - Private Properties
    
    private let viewModel: OAuthViewModel!
    
    fileprivate lazy var fbLoginButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(handleFBLogin), for: .touchUpInside)
        button.backgroundColor = .blue
        button.setTitleColor(.white, for: .normal)
        button.setTitle("FB Login", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.titleLabel?.textAlignment = .center
        return button
    }()
    
    fileprivate lazy var lineLoginButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(handleLineLogin), for: .touchUpInside)
        button.backgroundColor = .green
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Line Login", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.titleLabel?.textAlignment = .center
        return button
    }()
    
    fileprivate lazy var googleLoginButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(handleGoogleLogin), for: .touchUpInside)
        button.backgroundColor = .red
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Google Login", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.titleLabel?.textAlignment = .center
        return button
    }()
    
    fileprivate let messageTextView: UITextView = {
        let textView = UITextView()
        textView.text = "Welcome"
        textView.textAlignment = .center
        textView.textColor = .white
        textView.backgroundColor = .lightGray
        return textView
    }()
    
    // MARK: - Initialization
    
    init(viewModel: OAuthViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit { print("OAuthView deinit") }
    
    // MARK: - View Configuration
    
    private func setupViews() {
        backgroundColor = .white
        addSubview(messageTextView)
        messageTextView.anchor(top: nil, leading: leadingAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, trailing: trailingAnchor, padding: .init(top: 0, left: 30, bottom: 30, right: 30), size: .init(width: UIScreen.main.bounds.size.width, height: 0))
        
        let buttonControlsStackView = UIStackView(arrangedSubviews: [fbLoginButton, lineLoginButton, googleLoginButton])
        addSubview(buttonControlsStackView)
        buttonControlsStackView.axis = .vertical
        buttonControlsStackView.spacing = 30
        buttonControlsStackView.distribution = .fillEqually
        
        fbLoginButton.anchor(size: .init(width: 0, height: 50))
        buttonControlsStackView.anchor(top: safeAreaLayoutGuide.topAnchor, leading: leadingAnchor, bottom: messageTextView.topAnchor, trailing: trailingAnchor, padding: .init(top: 30, left: 30, bottom: 30, right: 30), size: .init(width: UIScreen.main.bounds.size.width, height: 0))
    }
    
    // MARK: - Button Actions
    
    @objc private func handleFBLogin() {
        delegate?.fbButtonTapped(viewModel: viewModel)
    }
    
    @objc private func handleLineLogin() {
        delegate?.lineButtonTapped(viewModel: viewModel)
    }
    
    @objc private func handleGoogleLogin() {
        delegate?.googleButtonTapped(viewModel: viewModel)
    }
    
    // MARK: - Public Functions
    
    func update(message text: String) {
        messageTextView.text = text
    }
}
