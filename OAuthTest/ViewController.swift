//
//  ViewController.swift
//  OAuthTest
//
//  Created by Jeffbala on 2018/11/18.
//  Copyright © 2018年 Jeffbala. All rights reserved.
//

import UIKit
import OAuthSwift

class ViewController: UIViewController {
    
    private let googleClientId = "256904099549-3g1as8epmass320ii8srb5sh1g641mff.apps.googleusercontent.com"
    private let bundleId = Bundle.main.bundleIdentifier!

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .yellow
        
        let button = UIButton()
        view.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .red
        button.setTitle("  Snapshot & Upload  ", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(oAuth2Facebook), for: .touchUpInside)
        NSLayoutConstraint.activate([
            button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            button.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
    
    @objc private func oAuth2Facebook() {
        let clientId = "887468734977065"
        let consumerSecret = "693af0775ab4528049737e75551dd2c1"
        
        let oauthswift = OAuth2Swift(
            consumerKey:    clientId,
            consumerSecret: consumerSecret,
            authorizeUrl:   "https://www.facebook.com/dialog/oauth",
            accessTokenUrl: "https://graph.facebook.com/oauth/access_token",
            responseType:   "code"
        )
        
//        self.oauthswift = oauthswift
//        oauthswift.authorizeURLHandler = getURLHandler()
//        let state = generateState(withLength: 20)
//        let _ = oauthswift.authorize(
//            withCallbackURL: URL(string: "https://oauthswift.herokuapp.com/callback/facebook")!, scope: "public_profile", state: state,
//            success: { credential, response, parameters in
//                self.showTokenAlert(name: serviceParameters["name"], credential: credential)
//                self.testFacebook(oauthswift)
//        }, failure: { error in
//            print(error.localizedDescription, terminator: "")
//        }
//        )
    }
    func testFacebook(_ oauthswift: OAuth2Swift) {
        let _ = oauthswift.client.get(
            "https://graph.facebook.com/me?",
            success: { response in
                let dataString = response.string!
                print(dataString)
        }, failure: { error in
            print(error)
        }
        )
    }
    
    
    
    @objc private func oAuth2Google() {
        let oauthswift = OAuth2Swift(
            consumerKey: googleClientId,
            consumerSecret: "",
            authorizeUrl: "https://accounts.google.com/o/oauth2/auth",
            accessTokenUrl: "https://accounts.google.com/o/oauth2/token",
            responseType: "code"
        )
        
        oauthswift.allowMissingStateCheck = true
        
        oauthswift.authorizeURLHandler = SafariURLHandler(viewController: self, oauthSwift: oauthswift)
        
        guard let rwURL = URL(string: "\(bundleId):/oauth2Callback") else { return }

        oauthswift.authorize(withCallbackURL: rwURL, scope: "https://www.googleapis.com/auth/drive", state: "", success: {
            (credential, response, parameters) in
            print("authorize response: \(response?.response)")
            oauthswift.client.get("https://www.googleapis.com/drive/v2/files", success: { (response) in
                print("get files response: \(response.response)")
                let data = String(data: response.data, encoding: .utf8)
                print("data: \(data)")
                self.presentAlert("Get files success", message: "Successfully uploaded!")
            }, failure: { (error) in
                print("get files error: \(error)")
                self.presentAlert("Get files error", message: error.localizedDescription)
            })
            
            
//            oauthswift.client.postImage("https://www.googleapis.com/upload/drive/v2/files",
//                                        parameters: parameters,
//                                        image: self.snapshot(),
//                                        success: {
//                                            //4
//                                            (response) in
//                                            print("postImage response: \(response.response)")
//
//                                            if let _ = try? JSONSerialization.jsonObject(with: response.data, options: []) {
//                                                self.presentAlert("Success", message: "Successfully uploaded!")
//                                            }
//            },
//                                        failure: {
//                                            (error) in
//                                            print("postImage error: \(error)")
//                                            self.presentAlert("PostImage Error", message: error.localizedDescription)
//            })
        }, failure: { (error) in
            self.presentAlert("Authorize Error", message: error.localizedDescription)
        })
    }
    
    private func snapshot() -> Data {
        let renderer = UIGraphicsImageRenderer(size: view.bounds.size)
        let fullScreenshot = renderer.image { ctx in
            view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        }
        UIImageWriteToSavedPhotosAlbum(fullScreenshot, nil, nil, nil)
        return fullScreenshot.jpegData(compressionQuality: 0.5) ?? Data()
    }
    
    private func presentAlert(_ title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
